https://gitlab.com/gr-rastreamento/traccar/4.2/raw/master/traccar-linux-64-4.2.zip

https://gitlab.com/gr-rastreamento/mods/modv700.2/raw/master/MODV12.zip

https://gitlab.com/gr-rastreamento/traccar/4.2/raw/master/traccar.xml

https://gitlab.com/gr-rastreamento/traccar/4.2/raw/master/default.xml

Resolução imagens:

logo3 = 240x64
logo2 = 125x200
logo = 125x45

%s/#404040/HEX/g

# REQUISITOS:

    APACHE (Opcional)
    JAVA JRE
    MYSQL

```
sudo apt-get install default-jre
sudo apt-get install default-jdk

sudo apt-get install apache2 -y

sudo apt-get install mysql-server
sudo mysql_secure_installation

sudo apt-get install unzip
    
sudo add-apt-repository ppa:ondrej/php
sudo apt-get update
sudo apt-get install php7.0 libapache2-mod-php7.0 php7.0-mcrypt php7.0-mysql php7.0-curl
```


Solicitar liberação do servidor nominatim para o Paulo

# MANUAL:

1 Passo:
    Baixar o traccar.run na pasta /tmp
    
2 Passo:
    Executar o traccar.run
    
3 Passo:
    Remover os arquivos traccar.xml e default.xml da pasta /opt/traccar/conf
    
4 Passo:
    Baixar os arquivos traccar.xml e default.xml deste projeto
    
5 Passo:
    Alterar o arquivo traccar.xml inserindo o a database, login e senha do banco
    
6 Passo:
    Alterar o arquivo default.xml modificando a chave georeference
    
7 Passo:
    Executar o traccar.
    

# 1. Criar o arquivo que vai ser usado como memoria swap
`sudo fallocate -l 2G /swapfile`

# 2. Definir as permissões corretas (rw somente pro root)
`sudo chmod 600 /swapfile`

# 3. Criar de fato a swap
`sudo mkswap /swapfile`

# 4. Ativar
`sudo swapon /swapfile`

# 5. Edita o arquivo /etc/fstab pra manter a montagem no proximo boot
`sudo vi /etc/fstab`

# 6. Adicionar essa linha no final do arquivo.
`/swapfile   none    swap    sw    0   0`

 
# 7. Ver o que vc fez
```
sudo swapon -s
free -h
```

# MYSQL 

```
vim /etc/mysql/mysql.conf.d/mysqld.cnf 
#bind-address  =  127.0.0.1 

GRANT ALL ON *.* TO 'root'@'%' IDENTIFIED BY '[senha]' WITH GRANT OPTION; 
```

# Server Time


`sudo dpkg-reconfigure tzdata`


# Zabbix Agent:

```
sudo apt-get install zabbix-agent

vim /etc/zabbix/zabbix_agentd.conf

# cat /etc/zabbix/zabbix_agentd.conf
# On line 97 - Specify Zabbix server IP Address
Server=192.168.10.2
# On line 138 - Specify Zabbix server ( For active checks)
ServerActive=192.168.10.2
# Set server hostname reported by Zabbix agent
Hostname=node-01.computingforgeeks.

visudo

zabbix ALL=NOPASSWD: ALL
```

Restart zabbix-agent after making the change:

```
$ sudo systemctl restart zabbix-agent
$ sudo systemctl status zabbix-agent
```

Editar web/release.html para alterar o title e o rodapé

# Logo em PNG:

```
vim web/app.min.js
:%s/logo.svg/logo.png/g
```


# Cor Traccar

```
vim web/app.css
:%s/#5fa2dd/#corhex/g
:%s/#bbbbbb/#corhex/g
```

Acrescentar:

```
.x-panel-header-default-vertical{
background-color: #d0d0d0!important;
}
.x-panel-header-default{
background-color: #d0d0d0!important;
border: 1px solid #d0d0d0!important;
}
.x-panel-header-default .x-tool-tool-el{
background-color: #d0d0d0!important;
}
.x-panel-header-title-default{
color: #ffffff!important;
}


.x-window-header-default-top{
background-color: #d0d0d0!important;
}
.x-window-header-default{
border-color: #d0d0d0!important;
background-color: #d0d0d0!important;
}
.x-window-header-default .x-tool-img{
background-color: #d0d0d0!important;
}
.x-window-header-title-default{
color: #ffffff!important;
}

.x-window-default {
border-color: #d0d0d0!important;
}
.x-btn-default-small {
border-color: #3e3e3c!important;
}
.x-btn-default-small {
background-color: #3e3e3c!important;
}
.x-btn-inner-default-small {
color: #f0f0f0!important;
}
```
